/*
 * Polar Bear Framework 1.0
 * Copyright(c) 2015 - Luco Ny Aina Yannick RAMAROMANANA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mg.luco.aias.polarbear;

import java.util.List;
import mg.luco.aias.polarbear.exception.EntityIdNotFoundException;

/**
 *
 * @author Luco
 * @since 1.0
 * @param <E>
 */
public interface IService<E> {

    /**
     *
     * @param e
     */
    void saveOrUpdate(E e);
    
    /**
     * 
     * @param e 
     */
    void insert(E e);

    /**
     *
     * @param e
     */
    void delete(E e);

    /**
     *
     * @return
     */
    List<E> findAll();

    /**
     *
     * @param id
     * @return
     * @throws EntityIdNotFoundException
     */
    E findById(Object id) throws EntityIdNotFoundException;

}
