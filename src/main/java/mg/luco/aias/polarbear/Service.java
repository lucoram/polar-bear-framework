/*
 * Polar Bear Framework 1.0
 * Copyright(c) 2015 - Luco Ny Aina Yannick RAMAROMANANA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mg.luco.aias.polarbear;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import javax.annotation.PostConstruct;
import mg.luco.aias.polarbear.exception.EntityIdNotFoundException;
import mg.luco.aias.polarbear.exception.FieldNotFoundException;

/**
 *
 * @author Luco
 * @param <E>
 * @param <D>
 * @since 1.0
 */
public abstract class Service<E, D extends IDAO> {

    /**
     *
     */
    protected D dao;

    /**
     *
     */
    private final List<String> associationsToLoad = new ArrayList<>();

    /**
     *
     */
    protected final Class<E> entity;

    /**
     *
     */
    private final List<RelatedEntityPredicate> relatedPredicates = new ArrayList<>();

    /**
     *
     * @param entity
     */
    public Service(Class<E> entity) {
        this.entity = entity;
        Entity.updateFieldsAndEntitiesIdsPools(entity);
    }

    /**
     *
     */
    @PostConstruct
    private void init() {
        setDAO();
    }

    /**
     *
     */
    protected abstract void setDAO();

    /**
     *
     * @param e
     */
    public void saveOrUpdate(E e) {
        dao.saveOrUpdate(e);
    }

    public void insert(E e){
        dao.insert(e);
    }
    
    /**
     *
     * @param e
     */
    public void delete(E e) {
        dao.delete(e);
    }

    /**
     *
     * @return
     */
    public List<E> findAll() {
        return dao.findAll(null);
    }

    /**
     *
     * @param id
     * @return
     * @throws EntityIdNotFoundException
     */
    public E findById(Object id) throws EntityIdNotFoundException {
        if (Entity.ENTITY_IDS.get(entity) == null || Entity.ENTITY_IDS.get(entity).isEmpty()) {
            throw new EntityIdNotFoundException("The ID for entity " + entity.getSimpleName() + " has not been found");
        }
        return (E) dao.findById(id, null);
    }

    /**
     *
     * @param fields
     * @throws FieldNotFoundException
     */
    protected void addAssociationsToLoad(List<String> fields) throws FieldNotFoundException {
        associationsToLoad.clear();
        if (fields != null) {
            for (String field : fields) {
                checkRelatedField(field);
            }
        }
        associationsToLoad.addAll(fields);
    }

    /**
     *
     * @param fields
     * @throws FieldNotFoundException
     */
    protected void addAssociationsToLoad(String... fields) throws FieldNotFoundException {
        associationsToLoad.clear();
        if (fields != null) {
            addAssociationsToLoad(Arrays.asList(fields));
        }
    }

    /**
     *
     * @return
     */
    protected List<String> getAssociationsToLoad() {
        return associationsToLoad;
    }

    /**
     *
     * @param relatedPredicates
     * @throws FieldNotFoundException
     */
    protected void addRelatedPredciates(List<RelatedEntityPredicate> relatedPredicates) throws FieldNotFoundException {
        this.relatedPredicates.clear();
        if (relatedPredicates != null) {
            for (RelatedEntityPredicate relatedPredicate : relatedPredicates) {
                checkRelatedField(relatedPredicate.getFieldName());
            }
        }
        this.relatedPredicates.addAll(relatedPredicates);
    }

    /**
     *
     * @param relatedPredicates
     * @throws FieldNotFoundException
     */
    protected void addRelatedPredciates(RelatedEntityPredicate... relatedPredicates) throws FieldNotFoundException {
        this.relatedPredicates.clear();
        if (relatedPredicates != null) {
            addRelatedPredciates(Arrays.asList(relatedPredicates));
        }
    }

    /**
     *
     * @param field
     * @throws FieldNotFoundException
     */
    private void checkRelatedField(String field) throws FieldNotFoundException {
        if (!Entity.FIELDS_POOL.contains(field)) {
            throw new FieldNotFoundException("The field \"" + field + "\" has not been found for any entity in the fields pool");
        } else if (!Entity.RELATED_ENTITIES.get(entity).containsKey(field)) {
            throw new FieldNotFoundException("The field \"" + field + "\" has not been found in the \"" + entity.getSimpleName() + "\" entity");
        }
    }

    /**
     *
     * @return
     */
    public List<RelatedEntityPredicate> getRelatedPredicates() {
        return relatedPredicates;
    }
}
