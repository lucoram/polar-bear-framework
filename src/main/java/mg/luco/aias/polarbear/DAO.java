/*
 * Polar Bear Framework 1.0
 * Copyright(c) 2015 - Luco Ny Aina Yannick RAMAROMANANA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mg.luco.aias.polarbear;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.ParameterizedType;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import mg.luco.aias.polarbear.exception.FieldNotFoundException;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Luco
 * @param <E>
 * @since 1.0
 */
public abstract class DAO<E> {

    @Autowired
    private SessionFactory sessionFactory;
    private Session session;
    protected final Class<E> entity;

    public DAO() {
        entity = (Class<E>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
        Entity.updateFieldsAndEntitiesIdsPools(entity);
    }

    /**
     *
     * @param entity
     */
    public void saveOrUpdate(E entity) {
        alter(entity, true);
    }

    /**
     *
     * @param entity
     */
    @Transactional
    public void insert(E entity) {
        openSession();
        session.beginTransaction();
        session.save(entity);
        session.getTransaction().commit();
        closeSession();
    }

    /**
     *
     * @param entity
     */
    public void delete(E entity) {
        alter(entity, false);
    }

    /**
     *
     * @param entity
     * @param saveOrUpdate
     */
    @Transactional
    private void alter(E entity, boolean saveOrUpdate) {
        openSession();
        session.beginTransaction();
        if (saveOrUpdate) {
            session.saveOrUpdate(entity);
        } else {
            session.delete(entity);
        }
        session.getTransaction().commit();
        closeSession();
    }

    /**
     *
     * @param associationsToLoad
     * @return
     */
    public List<E> findAll(List<String> associationsToLoad) {
        Criteria criteria = createCriteria();
        return (List<E>) find(criteria, false, associationsToLoad);
    }

    /**
     *
     * @param id
     * @param associationsToLoad
     * @return
     */
    public E findById(Object id, List<String> associationsToLoad) {
        Criteria criteria = createCriteria();
        criteria.add(Restrictions.eq(Entity.ENTITY_IDS.get(entity), id));
        return (E) find(criteria, true, associationsToLoad);
    }

    /**
     *
     * @param fieldName
     * @param predicate
     * @param associationsToLoad
     * @return
     * @throws mg.luco.aias.polarbear.exception.FieldNotFoundException
     */
    public List<E> findAllBy(String fieldName, Object predicate, List<String> associationsToLoad) throws FieldNotFoundException {
        return (List<E>) find(createPredicateCriteria(fieldName, predicate), false, associationsToLoad);
    }

    /**
     *
     * @param fieldName
     * @param predicate
     * @param associationsToLoad
     * @return
     * @throws mg.luco.aias.polarbear.exception.FieldNotFoundException
     */
    public E findOneBy(String fieldName, Object predicate, List<String> associationsToLoad) throws FieldNotFoundException {
        return (E) find(createPredicateCriteria(fieldName, predicate), true, associationsToLoad);
    }

    private Criteria createPredicateCriteria(String fieldName, Object predicate) throws FieldNotFoundException {
        try {
            if (entity.getDeclaredField(fieldName) != null) {
                Criteria criteria = createCriteria();
                criteria.add(Restrictions.eq(fieldName, predicate));
                return criteria;
            }
        } catch (NoSuchFieldException | SecurityException ex) {
            throw new FieldNotFoundException("The field \"" + fieldName + "\" has not been found for the given entity");
        }
        return null;
    }

    /**
     *
     * @param relatedPredicates
     * @param associationsToLoad
     * @return
     */
    public List<E> findByRelatedPredicates(List<RelatedEntityPredicate> relatedPredicates, List<String> associationsToLoad) {
        Criteria criteria = createCriteria();
        addPredicates(criteria, relatedPredicates);
        return (List<E>) find(criteria, false, associationsToLoad);
    }

    /**
     *
     * @param criteria
     * @param relatedPredicates
     */
    private void addPredicates(Criteria criteria, List<RelatedEntityPredicate> relatedPredicates) {
        if (relatedPredicates != null && !relatedPredicates.isEmpty()) {
            for (RelatedEntityPredicate predicate : relatedPredicates) {
                Criteria subCriteria = criteria.createCriteria(predicate.getFieldName());
                Set<String> referencesFieldsKeys = predicate.getReferenceFields().keySet();
                Criterion[] conjunctions = new Criterion[predicate.getReferenceFields().size()];
                int conjunctionIndex = 0;
                for (String referenceField : referencesFieldsKeys) {
                    updateConjunctions(predicate, referenceField, conjunctions, conjunctionIndex);
                    conjunctionIndex++;
                }
                if (predicate.getLogicalOperator().equals(LogicalOperator.AND)) {
                    subCriteria.add(Restrictions.and(conjunctions));
                } else {
                    subCriteria.add(Restrictions.or(conjunctions));
                }
            }
        }
    }

    /**
     *
     * @param predicate
     * @param referenceField
     * @param conjunctions
     * @param conjunctionIndex
     */
    private void updateConjunctions(RelatedEntityPredicate predicate, String referenceField, Criterion[] conjunctions, int conjunctionIndex) {
        Criterion[] criterionArray = new Criterion[predicate.getReferences().size()];
        int criterionArrayIndex = 0;
        for (Object reference : predicate.getReferences()) {
            switch ((ComparisonOperator) predicate.getReferenceFields().get(referenceField)) {
                case EMPTY:
                    criterionArray[criterionArrayIndex] = Restrictions.isEmpty(referenceField);
                    break;
                case NOT_EMPTY:
                    criterionArray[criterionArrayIndex] = Restrictions.isNotEmpty(referenceField);
                    break;
                case EQUALS:
                    criterionArray[criterionArrayIndex] = Restrictions.eq(referenceField, getterValue(referenceField, reference));
                    break;
                case NOT_EQUALS:
                    criterionArray[criterionArrayIndex] = Restrictions.ne(referenceField, getterValue(referenceField, reference));
                    break;
            }
            criterionArrayIndex++;
        }
        conjunctions[conjunctionIndex] = Restrictions.or(criterionArray);
    }

    /**
     *
     * @param field
     * @param entity
     * @return
     */
    private Object getterValue(String field, Object entity) {
        try {
            return new PropertyDescriptor(field, entity.getClass()).getReadMethod().invoke(entity);
        } catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | IntrospectionException ex) {
            Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     *
     * @param queryName
     * @param queryParameters
     * @return
     */
    public Object findByNamedQuery(String queryName, Map<String, Object> queryParameters) {
        return null;
    }

    /**
     *
     * @param criteria
     * @param uniqueResult
     * @param associationsToLoad
     * @return
     */
    public Object find(Criteria criteria, boolean uniqueResult, List<String> associationsToLoad) {
        return fetchResult(criteria, uniqueResult, associationsToLoad);
    }

    /**
     *
     * @param query
     * @param uniqueResult
     * @param associationsToLoad
     * @return
     */
    public Object find(Query query, boolean uniqueResult, List<String> associationsToLoad) {
        return fetchResult(query, uniqueResult, associationsToLoad);
    }

    /**
     *
     * @param queryString
     * @param uniqueResult
     * @param associationsToLoad
     * @return
     */
    public Object find(String queryString, boolean uniqueResult, List<String> associationsToLoad) {
        Query query = openSession().createQuery(queryString);
        return fetchResult(query, uniqueResult, associationsToLoad);
    }

    /**
     *
     * @param queryString
     * @param uniqueResult
     * @param associationsToLoad
     * @return
     */
    public Object findWithSQLQuery(String queryString, boolean uniqueResult, List<String> associationsToLoad) {
        Query query = openSession().createSQLQuery(queryString);
        return fetchResult(query, uniqueResult, associationsToLoad);
    }

    /**
     *
     * @param fetcher
     * @param uniqueResult
     * @param associationsToLoad
     * @return
     */
    @Transactional(readOnly = true)
    private Object fetchResult(Object fetcher, boolean uniqueResult, List<String> associationsToLoad) {
        Object result;
        if (uniqueResult) {
            result = (E) (fetcher instanceof Criteria ? ((Criteria) fetcher).uniqueResult() : ((Query) fetcher).uniqueResult());
        } else {
            result = (List<E>) (fetcher instanceof Criteria ? ((Criteria) fetcher).list() : ((Query) fetcher).list());
        }
        if (result != null) {
            initAssociationsLoading(result, associationsToLoad);
        }
        closeSession();
        return result;
    }

    /**
     *
     * @param result
     * @param associationsToLoad
     */
    private void initAssociationsLoading(Object result, List<String> associationsToLoad) {
        if (associationsToLoad != null && !associationsToLoad.isEmpty()) {
            if (result instanceof List) {
                for (E t : (List<E>) result) {
                    loadAssociations(t, associationsToLoad);
                }
            } else {
                loadAssociations((E) result, associationsToLoad);
            }
        }
    }

    /**
     *
     * @param foundEntity
     * @param associationsToLoad
     */
    private void loadAssociations(E foundEntity, List<String> associationsToLoad) {
        for (String field : associationsToLoad) {
            try {
                Hibernate.initialize(Entity.RELATED_ENTITIES.get(foundEntity.getClass()).get(field).invoke(foundEntity));
            } catch (SecurityException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
                Logger.getLogger(DAO.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     *
     * @return
     */
    private Session openSession() {
        session = sessionFactory.openSession();
        return session;
    }

    /**
     *
     */
    private void closeSession() {
        if (session != null) {
            session.close();
        }
    }

    /**
     *
     * @return
     */
    protected Criteria createCriteria() {
        return openSession().createCriteria(entity);
    }

    /**
     *
     * @param queryString
     * @return
     */
    protected Query createQuery(String queryString) {
        return openSession().createQuery(queryString);
    }

    /**
     *
     * @param queryString
     * @return
     */
    protected SQLQuery createSQLQuery(String queryString) {
        return openSession().createSQLQuery(queryString);
    }
}
