/*
 * Polar Bear Framework 1.0
 * Copyright(c) 2015 - Luco Ny Aina Yannick RAMAROMANANA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mg.luco.aias.polarbear;

import java.util.List;
import java.util.Map;

/**
 *
 * @author Luco
 * @param <E>
 * @since 1.0
 */
public class RelatedEntityPredicate<E> {

    /**
     *
     */
    private String fieldName;

    /**
     *
     */
    private List<E> references;

    /**
     *
     */
    private Map<String, ComparisonOperator> referenceFields;

    /**
     *
     */
    private LogicalOperator logicalOperator;

    /**
     *
     * @param fieldName
     * @param references
     * @param referenceFields
     * @param logicalOperator
     */
    public RelatedEntityPredicate(String fieldName, List<E> references, Map<String, ComparisonOperator> referenceFields, LogicalOperator logicalOperator) {
        this.fieldName = fieldName;
        this.references = references;
        this.referenceFields = referenceFields;
        this.logicalOperator = logicalOperator;
    }

    /**
     *
     */
    public RelatedEntityPredicate() {
    }

    /**
     *
     * @return
     */
    public String getFieldName() {
        return fieldName;
    }

    /**
     *
     * @param fieldName
     */
    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    /**
     *
     * @return
     */
    public List<E> getReferences() {
        return references;
    }

    /**
     *
     * @param references
     */
    public void setReferences(List<E> references) {
        this.references = references;
    }

    /**
     *
     * @return
     */
    public Map<String, ComparisonOperator> getReferenceFields() {
        return referenceFields;
    }

    /**
     *
     * @param referenceFields
     */
    public void setReferenceFields(Map<String, ComparisonOperator> referenceFields) {
        this.referenceFields = referenceFields;
    }

    /**
     *
     * @return
     */
    public LogicalOperator getLogicalOperator() {
        return logicalOperator;
    }

    /**
     *
     * @param logicalOperator
     */
    public void setLogicalOperator(LogicalOperator logicalOperator) {
        this.logicalOperator = logicalOperator;
    }

}
