/*
 * Polar Bear Framework 1.0
 * Copyright(c) 2015 - Luco Ny Aina Yannick RAMAROMANANA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mg.luco.aias.polarbear;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EmbeddedId;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Transient;
import org.apache.commons.lang3.ArrayUtils;

/**
 *
 * @author Luco
 * @since 1.0
 */
@MappedSuperclass
public abstract class Entity {

    /**
     *
     */
    @Transient
    public static final Map<Class, Map<String, Method>> RELATED_ENTITIES = new HashMap<>();

    /**
     *
     */
    @Transient
    public static final Set<String> FIELDS_POOL = new HashSet<>();

    /**
     *
     */
    @Transient
    public static final Map<Class, String> ENTITY_IDS = new HashMap<>();

    /**
     *
     */
    @Transient
    private final Class entity;

    /**
     *
     */
    public Entity() {
        entity = this.getClass();
        updateFieldsAndEntitiesIdsPools(entity);
    }

    /**
     *
     * @param field
     * @param annotation
     * @param associatedFieldsGettersMap
     */
    private static boolean putAssociatedFieldGetter(Field field, Annotation annotation, Map<String, Method> associatedFieldsGettersMap, Class entity) {
        if (annotation instanceof OneToOne || annotation instanceof ManyToOne || annotation instanceof OneToMany || annotation instanceof ManyToMany) {
            try {
                associatedFieldsGettersMap.put(field.getName(), new PropertyDescriptor(field.getName(), entity).getReadMethod());
            } catch (IntrospectionException ex) {
                Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
            }
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param field
     * @param annotation
     */
    private static void putIds(Field field, Annotation annotation, Class entity) {
        if (annotation instanceof Id || annotation instanceof EmbeddedId) {
            ENTITY_IDS.put(entity, field.getName());
        }
    }

    /**
     *
     * @return
     */
    private Object getObjectId() {
        try {
            return new PropertyDescriptor(ENTITY_IDS.get(entity), entity).getReadMethod().invoke(this);
        } catch (IntrospectionException | IllegalAccessException | IllegalArgumentException | InvocationTargetException ex) {
            Logger.getLogger(Entity.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    /**
     *
     * @param entity
     */
    static void updateFieldsAndEntitiesIdsPools(Class entity) {
        if (!RELATED_ENTITIES.containsKey(entity)) {
            Field[] fields = getEntityFields(entity);
            Map<String, Method> associatedFieldsGettersMap = new HashMap<>();
            boolean related = false;
            for (Field field : fields) {
                Annotation[] annotations = field.getAnnotations();
                for (Annotation annotation : annotations) {
                    if (!related) {
                        related = putAssociatedFieldGetter(field, annotation, associatedFieldsGettersMap, entity);
                    } else {
                        putAssociatedFieldGetter(field, annotation, associatedFieldsGettersMap, entity);
                    }
                    putIds(field, annotation, entity);
                }
                FIELDS_POOL.add(field.getName());
            }
            if (related) {
                RELATED_ENTITIES.put(entity, associatedFieldsGettersMap);
            }
        }
    }

    private static Field[] getEntityFields(Class entity){
        Annotation[] entityAnnotations = entity.getAnnotations();
        Field[] declaredFields = entity.getDeclaredFields();
        Field[] inheritedFields = null;
        for(Annotation annotation : entityAnnotations){
            if(annotation instanceof PrimaryKeyJoinColumn){
                inheritedFields = entity.getSuperclass().getDeclaredFields();
                break;
            }
        }
        if(inheritedFields != null){
            return ArrayUtils.addAll(inheritedFields, declaredFields);
        }else{
            return declaredFields;
        }
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        Object id = getObjectId();
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof Entity)) {
            return false;
        }
        Entity other = (Entity) object;
        Object thisId = getObjectId();
        Object othersId = other.getObjectId();
        return !((thisId == null && othersId != null) || (thisId != null && !thisId.equals(othersId)));
    }

    @Override
    public String toString() {
        return entity.getPackage() + "." + entity.getSimpleName() + "[ id=" + getObjectId() + " ]";
    }

}
