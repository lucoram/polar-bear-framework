/*
 * Polar Bear Framework 1.0
 * Copyright(c) 2015 - Luco Ny Aina Yannick RAMAROMANANA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mg.luco.aias.polarbear.exception;

/**
 *
 * @author Luco
 * @since 1.0
 */
public class EntityIdNotFoundException extends Exception {

    /**
     * Creates a new instance of <code>EntityIdNotFoundException</code> without
     * detail message.
     */
    public EntityIdNotFoundException() {
    }

    /**
     * Constructs an instance of <code>EntityIdNotFoundException</code> with the
     * specified detail message.
     *
     * @param msg the detail message.
     */
    public EntityIdNotFoundException(String msg) {
        super(msg);
    }
}
