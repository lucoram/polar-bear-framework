/*
 * Polar Bear Framework 1.0
 * Copyright(c) 2015 - Luco Ny Aina Yannick RAMAROMANANA
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package mg.luco.aias.polarbear;

/**
 *
 * @author Luco
 * @since 1.0
 */
public enum ComparisonOperator {

    /**
     *
     */
    EMPTY(1),

    /**
     *
     */
    NULL(1),

    /**
     *
     */
    NOT_EMPTY(1),

    /**
     *
     */
    NOT_NULL(1),

    /**
     *
     */
    EQUALS(2),

    /**
     *
     */
    NOT_EQUALS(2),

    /**
     *
     */
    SMALLER_OR_EQUALS(2),

    /**
     *
     */
    STRICTLY_SMALLER(2),

    /**
     *
     */
    GREATER_OR_EQUALS(2),

    /**
     *
     */
    STRICTLY_GREATER(2),

    /**
     *
     */
    LIKE(2),

    /**
     *
     */
    ILIKE(2),

    /**
     *
     */
    BETWEEN(3),

    /**
     *
     */
    MATCH_LIKE(3),

    /**
     *
     */
    MATCH_ILIKE(3);

    /**
     *
     */
    private final int argsNumber;

    /**
     *
     * @param argsNumber
     */
    private ComparisonOperator(int argsNumber) {
        this.argsNumber = argsNumber;
    }

    /**
     *
     * @return
     */
    public int getArgsNumber() {
        return argsNumber;
    }
}
