# ABOUT #

Polar Bear is a Spring/Hibernate based Java framework intended to facilitate the hibernate lazy loading of related entities and provide the most common spring DAO methods such as find findById(), findAll(), insert(), saveOrUpdate(), etc. all shared within a super abstract class. Polar Bear will evolve to become a much more complete Spring/Hibernate based framework which will include datasource routing, field search filtering and sorting with predicates, automated database related tasks and much more... So why "Polar Bear"? Because "Polar Bears Hibernate on winter awaiting for Spring"

* Repository for polarbearframework project
* Version 1.0
* [Polar Bear](https://bitbucket.org/lucoram/polar-bear-framework)

### Setup ###

* Install JDK 7 and netbeans 8
* Install easyUML plugin for netbeans for viewing the UML diagrams (optional)
* clone this project from https://bitbucket.org/lucoram/winter in netbeans
* open and build it (right click on the project in projects explorer -> build) and include the jar to the project of your choice, or add this project as maven dependency to the other projects

### Contribution guidelines ###

* Unit tests are to be written in the dependent projects
* Code review will be done by the initial author of the project (Luco RAMAROMANANA)
* Follow Java best practices
* Apply git [best practices](http://www.git-tower.com/learn/git/ebook/command-line/appendix/best-practices)

### Who do I talk to? ###

* Admin: Luco RAMAROMANANA